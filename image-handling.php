<?php
/*
Plugin Name:  Image Handling Configs
Plugin URI:   https://jumpgroup.it/
Description:  Adds Configs for Image Handling
Version:      1.1.0
Author:       Jump Group
License:      MIT License
*/

namespace JumpGroup\ImageHanding;
use JumpGroup\ImageHanding\AddMimes;
use JumpGroup\ImageHanding\MediaCloudConfig;

if ( ! defined( 'WPINC' ) ) {
	die;
}

class Init{

  protected static $instance;

    public static function get_instance(){
      if( null == self::$instance ){
        self::$instance = new self();
      }
      return self::$instance;
    }

    protected function __construct(){
      AddMimes::init();
      MediaCloudConfig::init();
    }
}


$instance = Init::get_instance();
