<?php
namespace JumpGroup\ImageHanding;
class MediaCloudConfig{
    public static function init() {

      add_action('muplugins_loaded', function() {
        $s3_prefix = '/sites/'.env('S3_SITE_BUCKET').'/uploads/@{date:Y/m}';
        $cdn_base = 'https://'.env('S3_MAIN_BUCKET').'.'.env('S3_MAIN_REGION').'.cdn.digitaloceanspaces.com';

        putenv("ILAB_MEDIA_S3_ENABLED=true");
        putenv("ILAB_CLOUD_STORAGE_PROVIDER=do");
        putenv("ILAB_AWS_S3_ACCESS_KEY=".env('S3_UPLOADS_KEY'));
        putenv("ILAB_AWS_S3_ACCESS_SECRET=".env('S3_UPLOADS_SECRET'));
        putenv("ILAB_AWS_S3_BUCKET=".env('S3_MAIN_BUCKET'));  
        putenv("ILAB_AWS_S3_ENDPOINT=".env('S3_UPLOADS_ENDPOINT'));
        // Path endpoint true?
        putenv("ILAB_MEDIA_S3_PREFIX=".$s3_prefix);
        putenv("ILAB_MEDIA_S3_PRIVACY=public-read");    
        putenv("ILAB_AWS_S3_CACHE_CONTROL=". 60 * 60 * 24 * 365 ); // a year
          //Content Expiration?
        putenv("ILAB_MEDIA_S3_UPLOAD_DOCUMENTS=true");
        putenv("ILAB_MEDIA_S3_IGNORED_MIME_TYPES=''");
        putenv("ILAB_MEDIA_S3_DELETE_UPLOADS=true");
        putenv("ILAB_MEDIA_S3_DELETE_FROM_S3=true");  
        putenv("ILAB_AWS_S3_CDN_BASE=".$cdn_base);
      });
      
    }
}
